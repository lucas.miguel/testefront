$(function(){
	//MENU
	$('.menu ul').on('click','li',function(){
		var teste = $(this).data('teste');
		$('.menu ul li').removeClass('active');
		$(this).addClass('active');
		$('.testes>div').hide();
		$('.testes>div#'+teste).fadeIn('fast');
	});

	//LOJA
	var tipo = '1,2,3';
	$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&llj=true&full=true&jsoncallback=?",dataType:"json"}).done(function(data){
		$.each(data.loja,function(i,loja){  
			if(i<=1){
				$('.loja_lista').append(
					'<li>'+	
						'<h4 class="text-center mb-4">'+loja.loja_nome+'</h4>'+
						'<p><b>LUC:</b> '+loja.loja_luc+'</p>'+
						'<p><b>E-mail:</b></p>'+
						'<p><b>Localização:</b> '+loja.loja_localizacao+'</p>'+
						'<p><b>Instagram:</b></p>'+
						'<p><b>WhatsApp:</b></p>'+
						'<p><b>Descrição:</b> '+loja.loja_texto+'</p>'+
					'</li>'
				);
				if(loja.loja_texto == ''){
					$('.loja_lista li p:last-child').hide();
				}
			}
		});
	});
	$(".btn-show").bind('click',function(e){
		var bt = $(".btn-show").attr('data-btn');//pegando valor de data para fazer o if
					
		if(bt == 1){//verificando valor para alterar text de btn
			$(".popup ").addClass('active');
			$(".btn-show").text('Mostrar');
			$(".btn-show").attr('data-btn',"2");
			
		}else if(bt == 2){//trocando texto de btn
			$(".popup ").removeClass('active');
			$(".btn-show").text('Esconder');
			$(".btn-show").attr('data-btn','1');
		}
		bt = $(".btn-show").attr('data-btn');		
	});
	$("#form-validacao").bind("submit",function(e){
		e.preventDefault();
		var fomrValid = $(this).filter(':input');
		validarForm(fomrValid);
		
		
	});
	$('.alert').hide();
	//hover em td
	$(".simplificada tbody tr ").hover(function(){
		$(this).addClass("position-relative");
		}, function(){
			$(this).removeClass("position-relative");
	});
	$(".simplificada tbody tr td").hover(function(){
		$(this).addClass("dadosTd");
		}, function(){
			$(this).removeClass("dadosTd");
	});

	//função para validar form
	function validarForm(dados,idForm){
		var i=0;
		var conteValor = 0;
		
		while(i<dados.context.length){
			var valor = $(dados.context[i]).val();
			if(valor == '' || valor == null){
				conteValor = conteValor + 2;//contando valores para ver se todos foram preenchidos ou nao 
				$(dados.context[i]).addClass('border-danger');
			}else{
				conteValor ++;
				$(dados.context[i]).removeClass('border-danger');
				$(dados.context[i]).addClass('border-success');
			}
			i++
		}
		var tudOk = validaCampos(dados);
		if(conteValor == i && tudOk == 1){//comparando campos se foram preenchidos, primeiro testa se todos foram preenchidos depois caso nao seja,exibi mensagem para usuario preencher
			$('.alert').removeClass('alert-danger');
			$('.alert').addClass('alert-success');
			$('.alert').show('');
			$('.alert').html('<p>Enviado com sucesso!</p>');
			if(!idForm == ""){
				enviarDados(idForm);
			}
			
		}else{
			$('.alert').removeClass('alert-success');
			$(dados.context[i]).addClass('border-danger');
			$('.alert').show('');
			$('.alert').html('<p>Preencha os campos em vermelho!</p>');
		}
		
	}
	//manipulando texto
	var textoDom = $(".texto").text();
	$(".texto").text(textoDom.substring(0,231)+'...');
	//cadastro de user
	$("#form-cadastro").bind("submit",function(e){
		e.preventDefault();
		var valida = $(this).filter(':input');
		validarForm(valida,"#form-cadastro");
		//enviarDados("#form-cadastro");		
	});
	function enviarDados(dadosForm){
		var dados = $(dadosForm).serialize();
		console.log(dados);
	
		$.ajax({
			type:"POST",
			url:"./querys/insert.php",
			data:dados,
			success:function(result){
				var status = parseInt(result);

				if(status == 1){
					$(".msg").html("<div class='alert alert-success' role='alert'>Cadastrado com sucesso!</div>");
				}else{
					$(".msg").html("<div class='alert alert-danger' role='alert'>Erro ao tentar cadastrar!</div>");
				}
				
			},
			error:function(){
				//console.log("deu ruim");
			}
		});
	
	}
	//funcao de validar alguns campos do form
	function validaCampos(valida){
		var i = 0;
		var status;
		while(i<valida.context.length){
			var valor = $(valida.context[i]).attr("name");//pegando name do campo da saber qual precisa fazer a validacao
			//iniciando validacao data de nascimento
			if(valor === 'datanasc' ){//pegando dados do input
				var dataNasc = $(valida.context[i]).val();
				
				if(isValidDate(dataNasc) == true){//testando quantidades de numeros com uma data sem / 
					$(valida.context[i]).addClass("border-success");//adicionando caso esteja tudo certo
					$(valida.context[i]).removeClass("border-danger");
					var maskdta = dataNasc.replace(/(\d{2})(\d{2})(\d{2})/, "$1/$2/$3"); //aplicando mask para o campo
					$(valida.context[i]).val(maskdta);
					status = 1;
				}else{
					$(valida.context[i]).addClass("border-danger")
					$(valida.context[i]).val('');
					status = 0;
				}
			}
			//testando e validando cpf
			if(valor === 'cpf'){
				var cpfDados = $(valida.context[i]);
				if(TestaCPF(cpfDados) == true){			
					$(valida.context[i]).addClass("border-success");
					$(valida.context[i]).removeClass("border-danger");
					var maskCpf = $(valida.context[i]).val().replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
					$(valida.context[i]).val(maskCpf);
					status = 1;
				}else {
					$(valida.context[i]).removeClass("border-success");
					$(valida.context[i]).addClass("border-danger");
					status = 0;
				}

				
			}
			//iniciando validacao de telefone e aplicando mask
			if(valor === 'telefone'){
				var telefoneDados = $(valida.context[i]).val();
				var telefoneNum = telefoneDados.replace(/[^0-9]/g, '').toString();
				
				if(telefoneNum.length == 11 ){//testando com dd e o 9
					$(valida.context[i]).addClass("border-success");
					$(valida.context[i]).removeClass("border-danger");
					var maskTel = telefoneNum.replace(/(\d{2})(\d{5})(\d{4})/, "$1-$2-$3");
					$(valida.context[i]).val(maskTel);
					status = 1;
				}else if(telefoneNum.length == 10){//testando sem o 9
					$(valida.context[i]).addClass("border-success");
					$(valida.context[i]).removeClass("border-danger");
					var maskTel = telefoneNum.replace(/(\d{2})(\d{4})(\d{4})/, "$1-$2-$3");
					$(valida.context[i]).val(maskTel);
					status = 1;
				}else{
					$(valida.context[i]).addClass("border-danger");
					status = 0;
				}

				
			}			
			i++
		}
		return status;
	}

	//validar cpf
	function TestaCPF(strCPF) {
		var exp = /\.|\-/g;
    
		var cpf = $(strCPF).val().replace(exp,'').toString();
		var status;
		if(cpf.length == 11 ){
		
		var v = [];

		//Calcula o primeiro dígito de verificação.
		v[0] = 1 * cpf[0] + 2 * cpf[1] + 3 * cpf[2];
		v[0] += 4 * cpf[3] + 5 * cpf[4] + 6 * cpf[5];
		v[0] += 7 * cpf[6] + 8 * cpf[7] + 9 * cpf[8];
		v[0] = v[0] % 11;
		v[0] = v[0] % 10;

		//Calcula o segundo dígito de verificação.
		v[1] = 1 * cpf[1] + 2 * cpf[2] + 3 * cpf[3];
		v[1] += 4 * cpf[4] + 5 * cpf[5] + 6 * cpf[6];
		v[1] += 7 * cpf[7] + 8 * cpf[8] + 9 * v[0];
		v[1] = v[1] % 11;
		v[1] = v[1] % 10;

		//Retorna Verdadeiro se os dígitos de verificação são os esperados.
				
		if ((v[0] != cpf[9]) || (v[1] != cpf[10])) {alert('CPF inválido ==> ' + cpf);$(strCPF);}
		
		else if (cpf[0] == cpf[1] && cpf[1] == cpf[2] && cpf[2] == cpf[3] && cpf[3] == cpf[4] && cpf[4] == cpf[5] && cpf[5] == cpf[6] && cpf[6] == cpf[7] && cpf[7] == cpf[8] && cpf[8] == cpf[9] && cpf[9] == cpf[10])
		{
			alert('CPF inválido ==> ' + cpf);$(strCPF).val('');
			status = false;
		}        
			
		else{
			$(strCPF);
			status = true;
		}       
			
		
		}else {
			$(strCPF).val('');
			status = false;
		} // 11
		return status;
	}

	
	function isValidDate(data)
	{
		reg = /[^\d\/\.]/gi;                  // Mascara = dd/mm/aaaa | dd.mm.aaaa
		var valida = data.replace(reg,'');    // aplica mascara e valida só numeros
		if (valida && valida.length == 10) {  // é válida, então ;)
		  var ano = data.substr(6),
			mes = data.substr(3,2),
			dia = data.substr(0,2),
			M30 = ['04','06','09','11'],
			v_mes = /(0[1-9])|(1[0-2])/.test(mes),
			v_ano = /(19[1-9]\d)|(20\d\d)|2100/.test(ano),
			rexpr = new RegExp(mes),
			fev29 = ano % 4? 28: 29;
  
		  if (v_mes && v_ano) {
			if (mes == '02') return (dia >= 1 && dia <= fev29);
			else if (rexpr.test(M30)) return /((0[1-9])|([1-2]\d)|30)/.test(dia);
			else return /((0[1-9])|([1-2]\d)|3[0-1])/.test(dia);
		  }
		}
		return false ;
	}
	
});



    
    
            
        