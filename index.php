<?php
    include './querys/getUsers.php';
?>
<!doctype html>
<html lang="pt-br" ng-app="myApp" ng-controller="AppCtrl">
	<head>	
		<meta charset="utf-8">
		<title>Teste Front-End | Madnezz</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <meta http-equiv="Content-Language" content="pt-br">
		<link rel="shortcut icon" href="img/favicon.ico" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="slider/dist/css/swiper.min.css">
        <link rel="stylesheet" href="dist/hamburgers.min.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">

		<!--== Google Fonts ==-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

		<!--== Default Madnezz -->

		<link rel="stylesheet" href="css/default.css" type="text/css" media="screen" />

        <!-- Questões -->
        
        <link rel="stylesheet" href="css/questoes.css" type="text/css" media="screen" />

		
        <script type="text/javascript" src="./js/jquery.js"></script>
		<script type="text/javascript" src="./js/lightbox.js"></script>        
		<script type="text/javascript" src="./js/functions.js"></script>      
		<script type="text/javascript" src="./js/bootstrap.min.js"></script>

        <script>
            var shopping_id = 5;
        </script>
	</head>
	<body>
        <div class="menu table-responsive">
            <ul>
                <li data-teste="html" class="active"><i class="fab fa-html5 mr-2"></i> HTML</li>
                <li data-teste="css"><i class="fab fa-css3-alt mr-2"></i> CSS</li>
                <li data-teste="jquery"><i class="fab fa-js-square mr-2"></i> jQuery</li>
                <li data-teste="banco"><i class="fas fa-database mr-2"></i> Banco de Dados</li>
            </ul>
        </div>

        <div class="conteudo">
            <div class="testes">
                <div id="html">
                    <h2 class="text-center">Teste HTML</h2>
                    <div class="questoes">
                        <div>
                            <div class="container table-responsive">
                                <span><b>1.</b> A tabela abaixo está desconfigurada devido ao número de colunas, faça com que todas as linhas ocupem o espaço necessário. Pode expandir qualquer coluna, o importante é que cada linha ocupe as 6 colunas.</span>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Coluna 1</th>
                                            <th>Coluna 2</th>
                                            <th>Coluna 3</th>
                                            <th>Coluna 4</th>
                                            <th>Coluna 5</th>
                                            <th>Coluna 6</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">Dado 1</td>
                                            <td colspan="2">Dado 2</td>
                                            <td colspan="2">Dado 3</td>
                                        </tr>
                                        <tr>
                                            <td>Dado 1</td>
                                            <td>Dado 2</td>
                                            <td>Dado 3</td>
                                            <td>Dado 4</td>
                                            <td>Dado 5</td>
                                            <td>Dado 6</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">Dado 1</td>
                                        </tr>
                                        <tr>
                                            <td>Dado 1</td>
                                            <td>Dado 2</td>
                                            <td>Dado 3</td>
                                            <td>Dado 4</td>
                                            <td colspan="2">Dado 5</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">Dado 1</td>
                                            <td colspan="3">Dado 2</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div>
                            <div class="container table-responsive">
                                <span><b>2.</b> A tabela abaixo está com as informações simplificadas devido ao espaço. Faça com que ao passar o mouse sobre cada um exiba o texto completo.</span>
                                <table class="simplificada">
                                    <thead>
                                        <tr>
                                            <th>Coluna 1</th>
                                            <th>Coluna 2</th>
                                            <th>Coluna 3</th>
                                            <th>Coluna 4</th>
                                            <th>Coluna 5</th>
                                            <th>Coluna 6</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Texto com muitos carcteres 1</td>
                                            <td>Texto com muitos carcteres 2</td>
                                            <td>Texto com muitos carcteres 3</td>
                                            <td>Texto com muitos carcteres 4</td>
                                            <td>Texto com muitos carcteres 5</td>
                                            <td>Texto com muitos carcteres 6</td>
                                        </tr>
                                        <tr>
                                            <td>Texto com muitos carcteres 1</td>
                                            <td>Texto com muitos carcteres 2</td>
                                            <td>Texto com muitos carcteres 3</td>
                                            <td>Texto com muitos carcteres 4</td>
                                            <td>Texto com muitos carcteres 5</td>
                                            <td>Texto com muitos carcteres 6</td>
                                        </tr>
                                        <tr>
                                            <td>Texto com muitos carcteres 1</td>
                                            <td>Texto com muitos carcteres 2</td>
                                            <td>Texto com muitos carcteres 3</td>
                                            <td>Texto com muitos carcteres 4</td>
                                            <td>Texto com muitos carcteres 5</td>
                                            <td>Texto com muitos carcteres 6</td>
                                        </tr>
                                        <tr>
                                            <td>Texto com muitos carcteres 1</td>
                                            <td>Texto com muitos carcteres 2</td>
                                            <td>Texto com muitos carcteres 3</td>
                                            <td>Texto com muitos carcteres 4</td>
                                            <td>Texto com muitos carcteres 5</td>
                                            <td>Texto com muitos carcteres 6</td>
                                        </tr>
                                        <tr>
                                            <td>Texto com muitos carcteres 1</td>
                                            <td>Texto com muitos carcteres 2</td>
                                            <td>Texto com muitos carcteres 3</td>
                                            <td>Texto com muitos carcteres 4</td>
                                            <td>Texto com muitos carcteres 5</td>
                                            <td>Texto com muitos carcteres 6</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>3.</b> Faça formatações para que o texto abaixo não pareça um único texto corrido. Deixar o título em negrito em uma linha separada, a observação em itálico e o link direcionando para o site em uma nova aba.</span>
                                <p class="mt-5">
                                    <h1>Título</h1>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    <i>OBS: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    Para mais informações, acesse: <a href="http://www.madnezz.com.br/" target="_blank"> www.madnezz.com.br.</a></i>
                                </p>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>4.</b> As imagens abaixo são cartazes que poderiam estar sendo exibidas dentro de uma página de cinema, porém, ambas estão apresentando erro, identifique o problema e corrija a imagem 1. Na imagem 2 mantenha o erro, mas exiba o nome do filme através do atributo responsável por isso na imagem para que o usuário tenha ciência de qual cartaz deveria estar sendo exibido, independente do erro apresentado.</span>
                                <div class="d-flex mt-5">
                                    <div class="flex-1 px-5">
                                        <img src="./img/cartaz-coringa.jpg" />
                                    </div>
                                    <div class="flex-1 px-5">
                                        <img src="img/cartaz-batman.jpg" alt="cartaz batman" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="css">
                    <h2 class="text-center">Teste CSS</h2>

                    <div class="questoes">
                        <div>
                            <div class="container">
                                <span><b>1.</b> Faça com que os itens abaixo fiquem lado a lado, contudo, dependendo do tamanho da tela os itens deverão quebrar automaticamente. O importante neste exercício é a responsividade dos quadrados.<br><b>Use o arquivo questoes.css dentro da pasta css.</b><br><i>(Obs: Rearranje o HTML e CSS dos blocos do jeito que desejar.</i>)</span>
                                <div class="flex">
                                    <div class="bloco bloco-marg"></div>
                                    <div class="bloco bloco-marg"></div>
                                    <div class="bloco"></div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>2.</b> Insira uma imagem qualquer no retângulo azul utilizando as propriedades de background<br><b>Use o arquivo questoes.css dentro da pasta css.</b><br><i>(Obs: O mesmo deve ser responsivo e não ultrapassar o tamanho do retângulo, não tem problema cortar a imagem.</i>)</span>
                                <div class="retangulo"></div>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>3.</b> Deixe o formulário a seguir similar ao da imagem a seguir.<br> <a href="img/formulario.jpg" target="_blank">Clique aqui para visualizar a imagem</a><br>Pode modificar tanto o CSS quando o arquivo HTML da forma que bem entender <br><b>Use o arquivo questoes.css dentro da pasta css.</b><br><i>(Obs: O mesmo deve ser um formulário responsivo, quaisquer efeitos adicionais (como hover/click), fica a sua escolha, o ideal é que o resultado final se pareça com a imagem linkada.</i>)</span>
                            
<!----------------------------- Início Formulário, pode editar da forma que bem entender ---------------------------------------------------------------------------->
                                <form action="" class="form">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <input type="text" name="contato_nome" class="no-border" placeholder="Nome">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <input type="text" name="contato_telefone" class="no-border" placeholder="Telefone">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <input type="text" name="contato_email" class="no-border" placeholder="E-mail">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <select name="contato_assunto" id="contato_assunto" class="no-border" >
                                                <option selected="" disabled="">Assunto</option>
                                                <option value="Cinema">Cinema</option>
                                                <option value="Dúvidas">Dúvidas</option>
                                                <option value="Lojas">Lojas</option>
                                                <option value="Outros">Outros</option>
                                            </select> 
                                        </div>
                                        <div class="col-12">
                                            <label for="message" class="sr-only">Message</label>
                                            <textarea name="contato_mensagem" cols="30" rows="2" class="no-border" placeholder="Mensagem"></textarea>
                                        </div>
                                        <div class="col-12">
                                            <button class="btn btn-bordered">Enviar</button>
                                        </div>
                                    </div>                                    
                                </form>

<!----------------------------- Fim Formulário ----------------------------------------------------------------------------------------------------------------------->

                                        
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>4.</b> Faça com que os blocos abaixo fiquem um do lado do outro com os 3 juntos ocupando toda a largura da div pai, inserir uma margem entre eles e manter a mesma altura dos 3, sem cravar o height, tudo de forma responsiva, independente do tamanho do conteúdo de cada um. Além disso, faça com que o conteúdo de dentro dos blocos fique centralizado tanto horizontalmente quanto verticalmente no box.</span>
                                <div class="boxes flex">
                                    <div>
                                        <h4>Título 1</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    </div>
                                    <div>
                                        <h4>Título 2</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                    <div>
                                        <h4>Título 3</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="jquery">
                    <h2 class="text-center">Teste jQuery</h2>

                    <div class="questoes">
                        <div>
                            <div class="container">
                                <span><b>1.</b> Os dados das lojas abaixo são puxados da nossa api de lojas. Faça com que as informações faltantes sejam exibidas, caso a informação não exista no banco, fazer com o que a legenda também não seja exibida.<br><i>(Ex: Caso o campo "Descrição esteja vazio, faça com que a legenda "Descrição:" não apareça.</i>):</span>
                                <ul class="loja_lista"></ul>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>2.</b> Faça com que o clique no botão exiba e esconda o pop-up ao lado, as classes para exibir/esconder o pop-up já estão no arquivo CSS. Fazer também com que o nome do botão troque de "Mostrar" para "Esconder" e vice-versa:</span>
                                <div class="d-flex flex-wrap mt-5">
                                    <div class="flex-1 d-flex align-items-center justify-content-center">
                                        <button class="btn-show" data-btn="1">Esconder</button>
                                    </div>
                                    <div class="flex-1 d-flex align-items-center justify-content-center">
                                        <div class="popup active">
                                            <h4 class="text-center mb-3">Pop-up</h4>
                                            <p class="text-center">Esse é um teste de conhecimento em jQuery</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>3.</b> Faça uma validação via JS do formulário abaixo, ao clicar em "Enviar", caso algum campo esteja em branco, exibir a mensagem "Preencha todos os campos", caso todos estejam preenchidos, exibir a mensagem "Enviado com sucesso!". Exiba a mensagem da forma que preferir.</span>
                                <form id="form-validacao" method="POST">
                                    <input type="text" name="nome" placeholder="Nome" />
                                    <input type="text" name="email" placeholder="E-mail" />
                                    <input type="text" name="telefone" placeholder="Telefone" />
                                    <select name="assunto">
                                        <option selected disabled>Assunto</option>
                                        <option value="Crítica">Crítica</option>
                                        <option value="Dúvida">Dúvida</option>
                                        <option value="Sugestão">Sugestão</option>
                                    </select>
                                    <textarea name="mensagem" placeholder="Mensagem"></textarea>
                                    <input type="submit" value="Enviar" class="mt-4 float-right" />
                                    <div class="clearfix">
                                    </div>
                                    <div class="alert alert-danger" role="alert"></div>
                                </form>
                            </div>

                            <script>
                                var texto = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
                                $('.box_txt').html(texto);
                            </script>
                        </div>

                        <div>
                            <div class="container">
                                <span><b>4.</b> O texto abaixo está vindo a partir de uma variável JS, muitas vezes quando o layout precisa ter um tamanho fixo, precisamos adaptar as informações para que as mesmas não fiquem quebradas como no exemplo abaixo. Utilize a função que preferir de jQuery para exibir apenas parte do texto, no final dele coloque reticências para informar o usuário que existe mais texto. Não precisa fazer nenhuma função para exibir o restante do texto, apenas esconda para não quebrar.<br><i>OBS: Faça com que seja exibido parte do texto, mas sem alterar o texto da varíavel.</i> </span>
                                <div class="box_txt texto">
                                    <p class="mb-0"></p>
                                </div>
                            </div>

                            <script>
                                var texto = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
                                $('.box_txt').html(texto);
                            </script>
                        </div>
                    </div>
                </div>
                <div id="banco">
                    <h2 class="text-center">Teste Banco de Dados</h2>

                    <div class="questoes">
                        <div>
                            <div class="container">
                                <span>
                                    <b>1.</b> Com o conhecimento que possui em HTML/CSS/jQuery/Banco, crie um formulário onde deve ser possivel cadastrar:<br>
                                    - Nome do Funcionário<br>
                                    - Data de Nascimento<br>
                                    - CPF<br>
                                    - Telefone<br>
                                    - Cargo<br><br>


                                    <form id="form-cadastro" method="POST">
                                        <input type="text" name="nome" placeholder="Nome" />
                                        <input type="text" name="datanasc" placeholder="Data de Nascimento" id="datanasc" />
                                        <input type="text" name="cpf" placeholder="Cpf" id="cpf" />
                                        <input type="text" name="telefone" placeholder="Telefone" id="telefone" />
                                        <input type="text" name="cargo" placeholder="Cargo" />
                                        <input type="submit" value="Enviar" name="enviar" class="mt-4 float-right" />
                                        <div class="clearfix">
                                        </div>
                                        
                                        <div class="msg"></div>
                                    </form>

                                    O formulário precisa ter validação de campos onde a data de nascimento deve ter o formato dd/mm/aaaa e convertida em datetime na inserção no banco, CPF precisa ter 11 dígitos, podendo ou não utilizar máscara para incluir os pontos e traço automaticamente. Telefone ter DDD, 8 dígitos e o 9º ser opcional. Em seguida faça uma listagem para exibir os funcionários cadastrados.<br><br>
                                    Utilize o banco abaixo que fornecemos para testes e crie uma <b>nova</b> tabela com as informações que serão inseridas (Utilize de preferência PHP para conexão com o banco):<br>
                                    <b>MySQL</b><br>
                                    <b>Servidor:</b> opmy0014.servidorwebfacil.com<br>
                                    <b>Porta:</b> 3306<br>
                                    <b>Login:</b> eduar_teste<br>
                                    <b>Senha:</b> _31vh4sM<br>
                                    <b>Base:</b> eduardo50_teste
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Nome</th>
                                                    <th scope="col">Data Nascimento</th>
                                                    <th scope="col">Cpf</th>
                                                    <th scope="col">Telefone</th>
                                                    <th scope="col">Criado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php while($resultado = $result->fetch_object()): ?>
                                                    <tr>
                                                        <td><?php echo $resultado->id ?></td>
                                                        <td><?php echo $resultado->nome ?></td>
                                                        <td><?php echo date("d/m/Y", strtotime($resultado->data_nasc)) ?></td>
                                                        <td><?php echo  mask($resultado->cpf,'###.###.###-##') ?></td>
                                                        <td><?php echo $resultado->cargo?></td>
                                                        <td><?php echo $resultado->created ?></td>
                                                    </tr>

                                                <?php endwhile ?>                                                 
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>